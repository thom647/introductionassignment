# IntroductionAssignment

# Development assignment

This project allows Olyx to determine your experience in web development and to test how you come up with solutions and your way of thinking.

Try to not spend more than 4 hours doing this assignment.

## The assignment

You will build a create *React* App to show data from [the rick and morty api](https://rickandmortyapi.com/graphql) (or any other preferred api). We encourage you to try to do it in your own way, experiment and try to have fun! It's allowed to make assumptions about the assignment.

Depending on your interests you can focus more on the data or on the aesthetics of the application.

1. Make a dashboard which shows all the rick and morty episodes.  
2. Make each of these episodes clickable, to view more details about the episodes.
3. Make sure each of these episodes has a detail page and an unique url

Extra (For fun 😄 , only when you have time left you can pick one or two)

- Make an overview of all the characters in rick and morty, including filter(s).
Bonus points for storing filters dynamically in the URL
- How do you show components/items based on load/scroll
- Case grid – List and grid view toggle
- Add an infinite scroll
- Improve performance: Lazyload stuff, WebP etc.
- Turn it into a progressive web app

Or add anything you think is cool or nice!

## Submitting your code

When you complete the assignment either push your local working copy to this remote repository or email us a .zip file with your solution.

If you have any questions you can call or send us an email!
